<?php

namespace Database\Seeders;

use App\Models\Upload;
use Illuminate\Database\Seeder;

class ImageSeeder extends Seeder
{
    public function run()
    {

        // course images
        Upload::create([
            'type' => 'image',
            'name' => '2023-04-13-to5axxqasson-original.png',
            'original' => 'uploads/course/thumbnail/thumbnail2023-04-13-to5axxqasson-original.png',
            'paths' => json_decode('{
                "100x100": "uploads/course/thumbnail/thumbnail2023-04-13-mrapgambbsqb-1.webp",
                "300x300": "uploads/course/thumbnail/thumbnail2023-04-13-ssemawxwatwi-2.webp",
                "600x600": "uploads/course/thumbnail/thumbnail2023-04-13-zoteupq6k2af-3.webp"
            }'),
        ]);

        Upload::create([
            'type' => 'image',
            'name' => '2023-04-13-67gbgvli0ooj-original.png',
            'original' => 'uploads/course/thumbnail/thumbnail2023-04-13-67gbgvli0ooj-original.png',
            'paths' => json_decode('{
                "100x100": "uploads/course/thumbnail/thumbnail2023-04-13-gnyqm9ok9rcf-1.webp",
                "300x300": "uploads/course/thumbnail/thumbnail2023-04-13-rppkxjknxjfk-2.webp",
                "600x600": "uploads/course/thumbnail/thumbnail2023-04-13-ad2l5d0jbgk6-3.webp"
            }'),
        ]);

        Upload::create([
            'type' => 'image',
            'name' => '2023-04-13-5r22kjvcbfkk-original.png',
            'original' => 'uploads/course/thumbnail/thumbnail2023-04-13-5r22kjvcbfkk-original.png',
            'paths' => json_decode('{
                "100x100": "uploads/course/thumbnail/thumbnail2023-04-13-qxfmj1njozsw-1.webp",
                "300x300": "uploads/course/thumbnail/thumbnail2023-04-13-vwljkji1m1l2-2.webp",
                "600x600": "uploads/course/thumbnail/thumbnail2023-04-13-yikrimugcafa-3.webp"
            }'),
        ]);
        Upload::create([
            'type' => 'image',
            'name' => '2023-04-13-0r9v3tr0wq9v-original.png',
            'original' => 'uploads/course/thumbnail/thumbnail2023-04-13-0r9v3tr0wq9v-original.png',
            'paths' => json_decode('{
                "100x100": "uploads/course/thumbnail/thumbnail2023-04-13-urqgtr9aojjh-1.webp",
                "300x300": "uploads/course/thumbnail/thumbnail2023-04-13-mo6blahalsqk-2.webp",
                "600x600": "uploads/course/thumbnail/thumbnail2023-04-13-hkoqrf1vfwdi-3.webp"
            }'),
        ]);

        // slider images
        Upload::create([
            'type' => 'image',
            'name' => '2023-04-13-3jkxoroofhj3-original.jpeg',
            'original' => 'uploads/Slider/image/images2023-04-13-3jkxoroofhj3-original.jpeg',
            'paths' => json_decode('{
                "400x1260": "uploads/Slider/image/images2023-04-13-jtaqj3rbr0ww-1.webp"
            }'),
        ]);
        Upload::create([
            'type' => 'image',
            'name' => '2023-04-13-bwzfnilgbx8b-original.jpeg',
            'original' => 'uploads/Slider/image/images2023-04-13-bwzfnilgbx8b-original.jpeg',
            'paths' => json_decode('{
                "400x1260": "uploads/Slider/image/images2023-04-13-lak8ajwixm1l-1.webp"
            }'),
        ]);
    }
}
