<?php

namespace App\Providers;

use App\Lib\Repository\Abstracts\BaseRepository;
use App\Lib\Repository\Contracts\IBaseRepository;
use Illuminate\Support\Facades\URL;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->bindClasses();

        if(env('APP_HTTPS')==true){
            URL::forceScheme('https');
        }
        Paginator::useBootstrap();
    }

    public function bindClasses(): void
    {
        $this->app->bind(IBaseRepository::class, BaseRepository::class);
    }
}
