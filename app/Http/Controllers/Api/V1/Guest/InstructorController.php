<?php

namespace App\Http\Controllers\Api\V1\Guest;

use App\Models\User;

use Illuminate\Http\Request;
use App\Traits\FileUploadTrait;
use App\Http\Controllers\Controller;
use App\Traits\ApiReturnFormatTrait;
use App\Interfaces\LanguageInterface;
use Modules\Course\Interfaces\CourseInterface;
use Modules\Course\Interfaces\CourseCategoryInterface;
use Modules\Instructor\Interfaces\InstructorInterface;
use Modules\Instructor\Transformers\InstructorCollection;


class InstructorController extends Controller
{
    use ApiReturnFormatTrait, FileUploadTrait;

    protected $instructor;
    protected $user;


    public function __construct(
        InstructorInterface $instructor,
        User $user
    ) {
        $this->instructor   = $instructor;
        $this->user         = $user;
    }


    public function index(Request $request)
    {
        try {

            $search = $request->search;
            $gender = $request->gender;


            $instructors = $this->instructor->model()->with('user', 'user.image')->when($search, function ($query, $search) {
                return $query->whereHas('user', function ($query) use ($search) {
                    $query->where('name', 'like', '%' . $search . '%')->orWhere('email', 'like', '%' . $search . '%')->orWhere('phone', 'like', '%' . $search . '%');
                });
            })->when($gender, function ($query, $gender) {
                $query->where('gender', $gender);
            })->get();

            $data['instructors'] = new InstructorCollection($instructors);

            if ($data['instructors']) {
                return $this->responseWithSuccess(___('course.data found'), $data);
            }

            return $this->responseWithError(___('course.No data found'));
        } catch (\Throwable $th) {

            return $this->responseWithError(___('alert.something_went_wrong_please_try_again'), [], 400); // return error response
        }
    }


    public function deatils(Request $request)
    {

        try {

            $data['instructor'] = $this->instructor->model()->with('user', 'user.image')->where('id', $request->id)->first();

            if ($data['instructor']) {
                return $this->responseWithSuccess(___('course.data found'), $data);
            }

            return $this->responseWithError(___('course.No data found'));
        } catch (\Throwable $th) {

            return $this->responseWithError(___('alert.something_went_wrong_please_try_again'), [], 400); // return error response
        }
    }
}
