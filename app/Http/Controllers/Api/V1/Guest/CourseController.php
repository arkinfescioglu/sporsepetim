<?php

namespace App\Http\Controllers\Api\V1\Guest;

use Termwind\Components\Dd;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Traits\ApiReturnFormatTrait;
use App\Interfaces\LanguageInterface;
use Illuminate\Contracts\Support\Renderable;
use Modules\Course\Interfaces\CourseInterface;
use Modules\Course\Http\Requests\CourseRequest;
use Modules\Course\Transformers\CourseCollection;
use Modules\Course\Interfaces\CourseCategoryInterface;
use Modules\Course\Transformers\CourseDetailsResource;

class CourseController extends Controller
{
    use ApiReturnFormatTrait;


    // constructor injection
    protected $course;
    protected $courseCategory;
    protected $language;

    public function __construct(
        CourseInterface $courseInterface,
        CourseCategoryInterface $courseCategoryInterface,
        LanguageInterface $languageInterface
        )
    {
        $this->course = $courseInterface;
        $this->courseCategory = $courseCategoryInterface;
        $this->language = $languageInterface;
    }
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {
        try {

            $courses = $this->course->model()->with('category')->filter($request)->paginate($request->show ?? 10); // data
            $courseArr =  new CourseCollection($courses);
            if($courseArr){
                return $this->responseWithSuccess(___('course.data found'), $courseArr);
            }

        } catch (\Throwable $th) {
            return $this->responseWithError(___('alert.something_went_wrong_please_try_again'), [], 400); // return error response
        }
    }


    public function courseDetails(Request $request){
        try {
            $course = $this->course->model()->with('category','instructor:id,name','reviews','noticeBoards','thumbnailImage','courseType')->find($request->id);
            $courseArr = new CourseDetailsResource($course);
            if($courseArr){
                return $this->responseWithSuccess(___('course.data found'), $courseArr);
            }

        } catch (\Throwable $th) {
            return $this->responseWithError(___('alert.something_went_wrong_please_try_again'), [], 400); // return error response
        }
    }

}
