<?php

namespace App\Http\Controllers\Api\V1\Student;

use App\Enums\Role;
use App\Models\User;
use App\Models\Country;
use Illuminate\Http\Request;
use App\Traits\CommonHelperTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Events\UserEmailVerifyEvent;
use App\Http\Controllers\Controller;
use App\Traits\ApiReturnFormatTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Modules\Order\Interfaces\NoteInterface;
use Modules\Order\Http\Requests\NoteRequest;
use Modules\Order\Interfaces\EnrollInterface;
use Modules\Course\Interfaces\BookmarkInterface;
use Modules\Student\Interfaces\StudentInterface;
use App\Http\Requests\api\bookmark\BookmarkStoreRequest;
use App\Http\Requests\api\bookmark\BookmarkDeleteRequest;


class StudentController extends Controller
{
    use ApiReturnFormatTrait, CommonHelperTrait;

    protected $user;
    protected $student;
    protected $enrollRepository;
    protected $noteRepository;
    protected $bookmarkRepository;
    protected $template = 'panel.student';

    public function __construct(
        User $user,
        StudentInterface $student,
        EnrollInterface $enrollRepository,
        BookmarkInterface $bookmarkRepository,
        NoteInterface $noteRepository
    ) {
        $this->user             = $user;
        $this->student          = $student;
        $this->enrollRepository = $enrollRepository;
        $this->noteRepository   = $noteRepository;
        $this->bookmarkRepository = $bookmarkRepository;
    }



    public function profile()
    {

        try {
            $data['title']      = ___('student.My Profile'); // title
            $data['student']    = $this->student->model()->with(['user','user.image','user.role','country'])->where('user_id', Auth::id())->first();

            if($data['student']){
                return $this->responseWithSuccess(___('student.data found'), $data);
            }

            return $this->responseWithError(___('alert.no data found'), [], 400); // return error response

        } catch (\Throwable $th) {

            return $this->responseWithError(___('alert.something_went_wrong_please_try_again'), [], 400); // return error response
        }
    }

    public function courses(Request $request)
    {
        try {
            $data['title']      = ___('student.Student Courses'); // title
            $data['enrolls']    = $this->enrollRepository->model()->where('user_id', Auth::id())->with('course:id,title,course_duration,course_category_id,slug,thumbnail')
                ->search($request)
                ->latest()
                ->paginate(10);
            return view($this->template . '.my_courses', compact('data'));
        } catch (\Throwable $th) {
            return redirect()->back()->with('danger', ___('alert.something_went_wrong_please_try_again'));
        }
    }


    // course bookmark
    public function bookmark(Request $request)
    {
        try {
            $data['title']      = ___('student.Bookmark Courses'); // title
            $data['bookmarks']    = $this->bookmarkRepository->model()->where('user_id', Auth::id())
                ->with('course:id,title,course_duration,course_category_id,slug,price,thumbnail,created_by,rating,total_review')
                ->search($request)
                ->latest()
                ->get();


            if($data['bookmarks']){
                return $this->responseWithSuccess(___('student.data found'), $data);
            }

            return $this->responseWithError(___('alert.no data found'), [], 400); // return error response
        } catch (\Throwable $th) {
            return $this->responseWithError(___('alert.something_went_wrong_please_try_again'), [], 400); // return error response

        }
    }

    public function bookmarkStore(BookmarkStoreRequest $request)
    {

        try {
            $result = $this->bookmarkRepository->store($request->course_id);
            if ($result->original['result']) {
                return $this->responseWithSuccess($result->original['message'], $result->original['data']);
            } else {
                return $this->responseWithError($result->original['message']);
            }
        } catch (\Throwable $th) {
            return $this->responseWithError(___('alert.something_went_wrong_please_try_again'), [], 400); // return error response
        }
    }
    public function bookmarkRemove(BookmarkDeleteRequest $request)
    {
        try {
            $result = $this->bookmarkRepository->destroy($request->course_id);

            if ($result->original['result']) {
                return $this->responseWithSuccess($result->original['message']);
            } else {
                return $this->responseWithError($result->original['message']);
            }
        } catch (\Throwable $th) {
            return $this->responseWithError(___('alert.something_went_wrong_please_try_again'), [], 400); // return error response
        }
    }
    // course bookmark

    public function leaderBoard()
    {
        try {
            $data['title']      = ___('student.Leader Board'); // title
            $data['students'] = $this->student->model()->orderBy('points','asc')->paginate(10);
            return view($this->template . '.leader_board', compact('data'));
        } catch (\Throwable $th) {
            return redirect()->back()->with('danger', ___('alert.something_went_wrong_please_try_again'));
        }
    }

    public function logout()
    {
        try {
            auth()->logout();

            return redirect()->route('home')->with('success', ___('alert.Student Log out successfully!!'));
        } catch (\Throwable $th) {
            return redirect()->back()->with('danger', ___('alert.something_went_wrong_please_try_again'));
        }
    }
}
