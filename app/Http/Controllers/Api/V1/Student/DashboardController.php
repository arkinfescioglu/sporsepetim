<?php

namespace App\Http\Controllers\Api\V1\Student;

use App\Models\User;
use Illuminate\Http\Request;
use App\Traits\CommonHelperTrait;
use App\Http\Controllers\Controller;
use App\Traits\ApiReturnFormatTrait;
use Modules\Order\Interfaces\EnrollInterface;
use Modules\Order\Interfaces\OrderInterface;
use Modules\Student\Interfaces\StudentInterface;

class DashboardController extends Controller
{
    use ApiReturnFormatTrait, CommonHelperTrait;

    protected $user;
    protected $studentRepository;
    protected $enrollRepository;
    protected $orderRepository;


    public function __construct(User $user,StudentInterface $studentRepository,EnrollInterface $enrollRepository, OrderInterface $orderRepository)
    {
        $this->user                         = $user;
        $this->studentRepository            = $studentRepository;
        $this->enrollRepository             = $enrollRepository;
        $this->orderRepository              = $orderRepository;
    }


    public function index(){
        try {
            $data['title']                  = ___('student.Dashboard'); // title
            $data['course_count']           = $this->enrollRepository->model()->where('user_id',auth()->id())->count();
            $data['purchase_amounts']       = $this->orderRepository->model()->where('user_id',auth()->id())->sum('total_amount');
            $data['badges']                 = $this->studentRepository->model()->where('user_id',auth()->id())->first()->badges;

           return $this->responseWithSuccess(___('student.data found'), $data);

        } catch (\Throwable $th) {
            return $this->responseWithError(___('alert.something_went_wrong_please_try_again'), [], 400); // return error response
        }

    }
}
