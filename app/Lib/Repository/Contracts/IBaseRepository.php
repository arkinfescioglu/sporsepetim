<?php

namespace App\Lib\Repository\Contracts;

use Illuminate\Database\Eloquent\Model;

interface IBaseRepository
{
    /**
     * returns model detail by given model id.
     *
     * @param $id
     * @param array $columns
     * @return mixed
     */
    public function getById($id, array $columns = ["*"]): mixed;

    /**
     * returns model detail by given model id. If not found give exception.
     *
     * @param $id
     * @param array $columns
     * @return mixed
     */
    public function getByIdOrError($id, array $columns = ["*"]): mixed;

    /**
     * Return all data.
     *
     * @param array $columns
     * @return mixed
     */
    public function all(array $columns = ["*"]): mixed;

    /**
     * Paginate model.
     *
     * @param int $page
     * @param array $columns
     * @return mixed
     */
    public function paginate(int $page, array $columns = ["*"]): mixed;

    /**
     * Delete data from DB by given data id.
     *
     * @param $id
     * @return bool
     */
    public function deleteById($id): bool;

    /**
     * Multiple delete data from DB by given data ids.
     *
     * @param array $ids
     * @return bool
     */
    public function deleteMultiByIds(array $ids): bool;

    /**
     * Return model object.
     *
     * @return mixed
     */
    public function getModel(): mixed;
}
