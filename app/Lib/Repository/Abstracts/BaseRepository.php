<?php

namespace App\Lib\Repository\Abstracts;

use App\Lib\Repository\Contracts\IBaseRepository;
use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

abstract class BaseRepository implements IBaseRepository
{
    /**
     * Elequent Model Object.
     *
     * @var Model $model
     */
    protected Model $model;

    /**
     * returns model detail by given model id.
     *
     * @param $id
     * @param array $columns
     * @return mixed
     */
    public function getById($id, array $columns = ["*"]): mixed
    {
        return $this->model->select($columns)->find($id);
    }

    /**
     * returns model detail by given model id. If not found give exception.
     *
     * @param $id
     * @param array $columns
     * @return mixed
     */
    public function getByIdOrError($id, array $columns = ["*"]): mixed
    {
        $model = $this->model->select($columns)->find($id);

        if(!$model)
            throw new NotFoundHttpException("Aradığınız veri bulunamadı.");

        return $model;
    }

    /**
     * Return all data.
     *
     * @param array $columns
     * @return mixed
     */
    public function all(array $columns = ["*"]): mixed
    {
        return $this->model->select($columns)->get();
    }

    /**
     * Paginate model.
     *
     * @param int $page
     * @param array $columns
     * @return mixed
     */
    public function paginate(int $page, array $columns = ["*"]): mixed
    {
        return $this->model->select($columns)->paginate($page);
    }

    /**
     * Delete data from DB by given data id.
     *
     * @param $id
     * @return bool
     */
    public function deleteById($id): bool
    {
        $model = $this->getByIdOrError($id, ["id"]);

        $model->delete();

        return true;
    }

    /**
     * Multiple delete data from DB by given data ids.
     *
     * @param array $ids
     * @return bool
     */
    public function deleteMultiByIds(array $ids): bool
    {
        if(is_array($ids)) {
            foreach ($ids as $id) {
                $this->deleteById($id);
            }
            return true;
        }
        return false;
    }

    /**
     * Set new model.
     *
     * @param $model
     * @return void
     */
    protected function resetModel($model): void
    {
        $this->model = $model;
    }

    /**
     * Return model object.
     *
     * @return mixed
     */
    public function getModel(): mixed
    {
        return $this->model;
    }
}
