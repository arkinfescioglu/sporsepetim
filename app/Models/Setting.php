<?php

namespace App\Models;

use App\Models\Upload;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Setting extends Model
{
    use HasFactory;

    // booted
    protected static function booted()
    {

        static::created(function ($settings) { // when courses created then forget cache
            cache()->forget('setting');
            cache()->forget('light_logo');
            cache()->forget('half_light_logo');
            cache()->forget('dark_logo');
            cache()->forget('half_dark_logo');
        });

        static::updated(function ($settings) { // when courses updated then forget cache
            cache()->forget('setting');
            cache()->forget('light_logo');
            cache()->forget('half_light_logo');
            cache()->forget('dark_logo');
            cache()->forget('half_dark_logo');
        });
    }


    public function lightLogo(){
        return $this->belongsTo(Upload::class, 'light_logo', 'id');
    }

    public function darkLogo()
    {
        return $this->belongsTo(Upload::class, 'dark_logo', 'id');
    }

    public function favicon()
    {
        return $this->belongsTo(Upload::class, 'favicon', 'id');
    }

}
