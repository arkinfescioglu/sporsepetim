@extends('backend.master')

@section('title')
    {{ @$data['title'] }}
@endsection
@section('content')
    <div class="page-content">

        {{-- breadecrumb Area S t a r t --}}
        @include('backend.ui-components.breadcrumb', [
            'title' => @$data['title'],
            'routes' => [
                route('dashboard') => ___('common.Dashboard'),
                route('languages.index') => ___('language.languages'),
                '#' => @$data['title'],
            ],
        
            'buttons' => 1,
        ])
        {{-- breadecrumb Area E n d --}}

        <div class="card ot-card">
            <div class="card-body">
                <form action="{{ route('languages.update.terms', @$data['language']->code) }}" enctype="multipart/form-data"
                    method="post" id="terms-form">
                    @csrf
                    @method('PUT')
                    <input type="hidden" name="code" id="code" value="{{ @$data['language']->code }}">
                    <div class="row mb-3">
                        <div class="col-md-12 mb-3">
                            <label for="validationServer04" class="form-label">{{ ___('language.module') }}</label>
                            <select class="select2 ot-input @error('lang_module') is-invalid @enderror change-module"
                                name="lang_module" id="validationServer04" aria-describedby="validationServer04Feedback">
                                @foreach ($data['modules'] as $key => $value)
                                    @php
                                        $explode = explode('.', $value);
                                    @endphp
                                    @if ($key > 1)
                                        @if ($explode[1] == 'json')
                                            <option value="{{ $value }}"
                                                @if ($value == 'common.json') selected @endif>
                                                {{ Str::title(substr($value, 0, -5)) }}</option>
                                        @endif
                                    @endif
                                @endforeach
                            </select>
                            @error('lang_module')
                                <div id="validationServer04Feedback" class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                        <div class="col-md-12">

                            <div class="row">
                                <div class="col-md-6">
                                    <label for="exampleDataList" class="form-label ">{{ ___('language.term') }}</label>
                                </div>
                                <div class="col-md-6">
                                    <label for="exampleDataList"
                                        class="form-label ">{{ ___('language.translated_language') }}</label>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 term-translated-language">
                            @foreach ($data['terms'] as $key => $row)
                                <div class="row mb-3">
                                    <div class="col-md-6">
                                        <input class="form-control ot-input" name="name" list="datalistOptions"
                                            id="exampleDataList" value="{{ $key }}" disabled>

                                    </div>
                                    <div class="col-md-6 translated_language">
                                        <input class="form-control ot-input" list="datalistOptions" id="exampleDataList"
                                            placeholder="{{ ___('language.translated_language') }}"
                                            name="{{ $key }}" value="{{ $row }}">
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        <div class="col-md-12 mt-3">
                            <div class="text-end">
                                <button class="btn btn-lg ot-btn-primary">{{ ___('common.submit') }}</button>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
@endsection
