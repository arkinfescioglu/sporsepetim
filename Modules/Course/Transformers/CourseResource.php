<?php

namespace Modules\Course\Transformers;

use Illuminate\Http\Resources\Json\JsonResource;

class CourseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */

    public function toArray($request)
    {
        return [
            'id' => @$this->id,
            'title' => @$this->title,
            'price' => @$this->price,
            'discount_price' => @$this->discount_price,
            'thumbnail' => @$this->thumbnail,
            'rating' => @$this->rating,
            'total_sales' => @$this->total_sales,
            'total_review' => @$this->total_review,
            'is_free' => @$this->is_free,
            'is_discount' => @$this->is_discount,
            'created_at' => @$this->created_at,
            'instructor' => @$this->instructor->name,
            'details' => route('home.api.course.details',@$this->id),
        ];
    }
}
