<?php

namespace Modules\Course\Transformers;

use Illuminate\Http\Resources\Json\JsonResource;

class CourseDetailsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => @$this->id,
            'title' => @$this->title,
            'slug' => @$this->slug,
            'short_description' => @$this->short_description,
            'description' => @$this->description,
            'requirements' => @$this->requirements,
            'outcomes' => @$this->outcomes,
            'faq' => @$this->faq,
            'tags' => @$this->tags,
            'course_overview_type' => @$this->course_overview_type,
            'video_url' => @$this->video_url,
            'language' => @$this->language,
            'price' => @$this->price,
            'is_discount' => @$this->is_discount,
            'discount_price' => @$this->discount_price,
            'discount_start_date' => @$this->discount_start_date,
            'discount_end_date' => @$this->discount_end_date,
            'is_free' => @$this->is_free,
            'rating' => @$this->rating,
            'total_sales' => @$this->total_sales,
            'total_review' => @$this->total_review,
            'is_free' => @$this->is_free,
            'course_duration' => @$this->course_duration,
            'created_at' => @$this->created_at,
            'instructor' => @$this->instructor->name,
            'category' => @$this->category,
            'course_type' => @$this->courseType,
            'reviews' => @$this->reviews,
            'notice_boards' => @$this->notice_boards,
            'thumbnail_image' => @$this->thumbnail_image,

        ];
    }
}
