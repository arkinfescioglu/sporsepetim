<?php

namespace Modules\Course\Repositories;

use App\Models\Setting;
use App\Models\Status;
use App\Models\User;
use App\Traits\ApiReturnFormatTrait;
use App\Traits\FileUploadTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Modules\CMS\Entities\FeaturedCourse;
use Modules\Course\Entities\Course;
use Modules\Course\Entities\CourseCategory;
use Modules\Course\Interfaces\CourseInterface;
use Modules\Course\Transformers\CourseResource;

class CourseRepository implements CourseInterface
{
    use ApiReturnFormatTrait, FileUploadTrait;

    private $model;
    protected $courseCategoryModel;
    protected $userModel;
    protected $statusModel;
    protected $setting;

    public function __construct(Course $courseModel, CourseCategory $courseCategoryModel, User $userModel, Status $statusModel, Setting $setting)
    {
        $this->model = $courseModel;
        $this->courseCategoryModel = $courseCategoryModel;
        $this->userModel = $userModel;
        $this->statusModel = $statusModel;
        $this->setting = $setting;
    }

    public function all()
    {
        try {
            return $this->model->get();
        } catch (\Throwable $th) {
            return $this->responseWithError(___('alert.something_went_wrong_please_try_again'));
        }
    }

    public function tableHeader()
    {

        return [
            ___('common.ID'),
            ___('common.Title'),
            ___('common.Category'),
            ___('common.Instructor'),
            ___('course.Content'),
            ___('course.Enrollment'),
            ___('common.price'),
            ___('ui_element.status'),
            ___('course.Visibility'),
            ___('ui_element.action'),
        ];
    }

    public function model()
    {

        return $this->model;
    }

    public function filter($filter = null)
    {
        $model = $this->model;
        if (@$filter) {
            $model = $this->model->where($filter);
        }
        return $model;
    }

    public function params($params = null)
    {
        $category = $params->category ?? null;
        $instructor = $params->instructor_id ?? null;
        $search = $params->search ?? null;
        return [
            'category' => $this->courseCategoryModel->active()->where('id', $category)->first()->title ?? null,
            'instructor' => $this->userModel->where('id', $instructor)->first()->name ?? null,
            'search' => $search,
        ];
    }

    public function courseDiscount($request)
    {
        try {

            $course = $this->model->find($request->course_id);
            if (!$course) {
                return $this->responseWithError(___('alert.Course_not_found.'));
            }
            if ($request->discount_type == 1 && $request->discount_price > $course->price) {
                return $this->responseWithError(___('alert.Discount_price_must_be_less_than_course_price.'));
            }
            $course->is_discount = 11;
            $course->discount_price = $request->discount_price;
            $course->discount_type = $request->discount_type;
            $course->save();
            return $this->responseWithSuccess(___('alert.Course_discount_updated_successfully.'));
        } catch (\Throwable $th) {
            return $this->responseWithError(___('alert.something_went_wrong_please_try_again'));
        }
    }

    public function courseDiscountDestroy($id)
    {
        try {
            $course = $this->model->find($id);
            if (!$course) {
                return $this->responseWithError(___('alert.Course_not_found.'));
            }
            $course->is_discount = 10;
            $course->discount_price = 0;
            $course->discount_type = null;
            $course->save();
            return $this->responseWithSuccess(___('alert.Course_discount_removed_successfully.'));
        } catch (\Throwable $th) {
            return $this->responseWithError(___('alert.something_went_wrong_please_try_again'));
        }
    }

    public function store($request)
    {

        DB::beginTransaction(); // start database transaction
        try {
            $courseModel = new $this->model; // create new object of model for store data in database table
            $courseModel->title = $request->title;
            $courseModel->slug = Str::slug($request->title) . '-' . Str::random(8);
            $courseModel->course_type = $request->course_type;
            $courseModel->level_id = $request->course_level;
            $courseModel->short_description = $request->short_description;
            $courseModel->description = $request->description;
            $courseModel->course_category_id = $request->category;
            $courseModel->visibility_id = $request->visibility_id;
            $courseModel->status_id = $request->status_id ?? 1;
            $courseModel->language = $request->language_id;
            $courseModel->requirements = $request->requirements;
            $courseModel->outcomes = $request->outcomes;
            $courseModel->is_free = $request->is_free ?? 0;
            $courseModel->price = @$request->price ?? 0;
            if (@$request->is_discount) {
                $courseModel->is_discount = $request->is_discount ?? 10;
                $courseModel->discount_price = $request->discount_price ?? 10;
                $courseModel->discount_type = $request->discount_type;
            }
            $courseModel->course_overview_type = $request->course_preview;
            $courseModel->video_url = $request->video_url ?? null;
            $courseModel->meta_title = $request->meta_title;
            $courseModel->meta_keywords = $request->meta_keyword;
            $courseModel->meta_author = auth()->user()->name;
            $courseModel->meta_description = $request->meta_description;
            $courseModel->instructor_id = $request->instructor;
            $courseModel->partner_instructors = @$request->partner_instructor;
            $courseModel->created_by = auth()->user()->id;
            $courseModel->updated_by = auth()->user()->id;
            // thumbnail upload
            if ($request->hasFile('thumbnail')) {
                $upload = $this->uploadFile($request->thumbnail, 'course/thumbnail/thumbnail', [[100, 100], [300, 300], [600, 600]], '', 'image'); // upload file and resize image 35x35
                if ($upload['status']) {
                    $courseModel->thumbnail = $upload['upload_id'];
                } else {
                    return $this->responseWithError($upload['message'], [], 400);
                }
            }
            // meta image upload
            if ($request->hasFile('meta_image')) {
                $upload = $this->uploadFile($request->meta_image, 'course/meta_image/meta_image', [], '', 'image'); // upload file and resize image 35x35
                if ($upload['status']) {
                    $courseModel->meta_image = $upload['upload_id'];
                } else {
                    return $this->responseWithError($upload['message'], [], 400);
                }
            }
            $courseModel->save(); // save data in database table
            DB::commit(); // commit database transaction
            return $this->responseWithSuccess(___('alert.Course created successfully.')); // return success response
        } catch (\Throwable $th) {
            DB::rollBack(); // rollback database transaction
            return $this->responseWithError($th->getMessage(), [], 400); // return error response
        }
    }

    public function show($id)
    {
        try {
            return $this->model->find($id);
        } catch (\Throwable $th) {
            return $this->responseWithError(___('alert.something_went_wrong_please_try_again'));
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $courseModel = $this->model->find($id);
            if (!$courseModel) {
                return $this->responseWithError(___('alert.Course not found.'), [], 400);
            }
            $courseModel->title = $request->title;
            if ($request->title != $courseModel->title) {
                $courseModel->slug = Str::slug($request->title) . '-' . Str::random(8);
            }
            $courseModel->course_type = $request->course_type;
            $courseModel->level_id = $request->course_level;
            $courseModel->short_description = $request->short_description;
            $courseModel->description = $request->description;
            $courseModel->course_category_id = $request->category;
            $courseModel->visibility_id = $request->visibility_id;
            $courseModel->status_id = $request->status_id ?? 1;
            $courseModel->language = $request->language_id;
            $courseModel->requirements = $request->requirements;
            $courseModel->outcomes = $request->outcomes;
            $courseModel->is_free = $request->is_free ?? 0;
            $courseModel->price = $request->price;
            if (@$request->is_discount) {
                $courseModel->is_discount = $request->is_discount ?? 10;
                $courseModel->discount_price = $request->discount_price ?? 10;
                $courseModel->discount_type = $request->discount_type;
            }
            $courseModel->course_overview_type = $request->course_preview;
            $courseModel->video_url = $request->video_url ?? null;
            $courseModel->meta_title = $request->meta_title;
            $courseModel->meta_keywords = $request->meta_keyword;
            $courseModel->meta_author = auth()->user()->name;
            $courseModel->meta_description = $request->meta_description;
            $courseModel->instructor_id = $request->instructor;
            $courseModel->partner_instructors = $request->partner_instructor;
            $courseModel->updated_by = auth()->user()->id;
            // thumbnail upload
            if ($request->hasFile('thumbnail')) {
                $upload = $this->uploadFile($request->thumbnail, 'course/thumbnail/thumbnail', [[100, 100], [300, 300], [600, 600]], $courseModel->thumbnail, 'image'); // upload file and resize image 35x35
                if ($upload['status']) {
                    $courseModel->thumbnail = $upload['upload_id'];
                } else {
                    return $this->responseWithError($upload['message'], [], 400);
                }
            }
            // meta image upload
            if ($request->hasFile('meta_image')) {
                $upload = $this->uploadFile($request->meta_image, 'course/meta_image/meta_image', [], $courseModel->meta_image, 'image'); // upload file and resize image 35x35
                if ($upload['status']) {
                    $courseModel->meta_image = $upload['upload_id'];
                } else {
                    return $this->responseWithError($upload['message'], [], 400);
                }
            }
            $courseModel->save(); // save data in database table
            DB::commit();
            return $this->responseWithSuccess(___('alert.Course updated successfully.'));
        } catch (\Throwable $th) {
            DB::rollBack();
            return $this->responseWithError($th->getMessage(), [], 400);
        }
    }

    public function destroy($id)
    {
        try {
            $courseModel = $this->model->find($id);
            $upload = $this->deleteFile($courseModel->thumbnail, 'delete'); // delete file from storage
            if (!$upload['status']) {
                return $this->responseWithError($upload['message'], [], 400); // return error response
            }
            $courseModel->delete();
            return $this->responseWithSuccess(___('alert.Course deleted successfully.')); // return success response
        } catch (\Throwable $th) {
            return $this->responseWithError($th->getMessage(), [], 400); // return error response
        }
    }

    // Home api

    public function getCoursedfds($request)
    {
        try {
            $allowArray = ['latest_courses', 'free_courses', 'featured_course'];

            $dataList = $this->model->query()->with('instructor:id,name')->select('id', 'title', 'price', 'discount_price', 'thumbnail', 'rating', 'total_sales', 'total_review', 'is_free', 'is_discount', 'created_by', 'created_at', 'updated_at')->get();

            $dataArr = $this->process_data($dataList, $allowArray);

            $data = $this->model->query()->with('instructor:id,name')->select('id', 'title', 'price', 'discount_price', 'thumbnail', 'rating', 'total_sales', 'total_review', 'is_free', 'is_discount', 'created_by', 'created_at', 'updated_at');

            if ($request->course_type) {
                $data = $this->filterCourse($request, $data);
            }

            return $this->responseWithSuccess(___('alert.data found.'), $dataArr); // return success response

        } catch (\Throwable $th) {
            return false;
        }
    }

    public function filterCourse($request, $data)
    {

        if ((setting('featured_courses') == 1) && ($request->course_type == 'featured')) {
            $featuredArr = $this->featuredCourse();
            return $data->whereIn('id', $featuredArr);
        }

        if ((setting('latest_courses') == 1) && ($request->course_type == 'latest')) {
            return $data->orderBy('id', 'desc');
        }

        if ((setting('best_rated_courses') == 1) && ($request->course_type == 'best_rated')) {
            return $data->where('rating', '>', 0)->orderBy('rating', 'desc');
        }

        if ((setting('best_selling_courses') == 1) && ($request->course_type == 'best_selling')) {
            return $data->where('total_sales', '>', 0)->orderBy('total_sales', 'desc');
        }

        if ((setting('free_courses') == 1) && ($request->course_type == 'free')) {
            return $data->where('is_free', 1);

        }

        if ((setting('discount_courses') == 1) && ($request->course_type == 'discount')) {
            return $data->where('is_discount', 11);
        }
    }

    public function featuredCourse()
    {
        try {
            return FeaturedCourse::active()->pluck('course_id', 'id')->toArray() ?? [];

        } catch (\Throwable $th) {
            return false;
        }

    }

    public function getCourse($request, $allowArray)
    {
        try {

            $dataList = $this->model->query()->with('instructor:id,name')->select('id', 'title', 'price', 'discount_price', 'thumbnail', 'rating', 'total_sales', 'total_review', 'is_free', 'is_discount', 'created_by', 'created_at', 'updated_at')->lazy();

            $finalData = $this->process_data($dataList, $allowArray);

            return $finalData ?? []; // return success response

        } catch (\Throwable $th) {
            return false;
        }
    }

    public function process_data($dataList, $allowArray)
    {

        try {

            $dataArr = [
                'featured_courses' => [],
                'latest_courses' => [],
                'best_rated_courses' => [],
                'best_selling_courses' => [],
                'free_courses' => [],
                'discount_courses' => [],

            ];

            foreach ($dataList as $data) {
                foreach ($allowArray as $allow_key) {
                    $arrayData = new CourseResource($data);

                    if ($allow_key == 'featured_courses' && in_array($data->id, $this->featuredCourse())) {
                        $dataArr[$allow_key][] = $arrayData;
                    }

                    if ($allow_key == 'latest_courses') {
                        array_unshift($dataArr[$allow_key], $arrayData);
                    }
                    if (($allow_key == 'best_rated_courses') && ($data->rating > 0)) {
                        $dataArr[$allow_key][] = $arrayData;
                    }
                    if (($allow_key == 'best_selling_courses') && ($data->total_sales > 0)) {
                        $dataArr[$allow_key][] = $arrayData;
                    }
                    if ($allow_key == 'free_courses' && $data->is_free == 1) {
                        $dataArr[$allow_key][] = $arrayData;
                    }
                    if ($allow_key == 'discount_courses' && $data->is_discount == 11) {
                        $dataArr[$allow_key][] = $arrayData;
                    }
                }

                // Sort the 'best_rated_courses' array by 'rating' in descending order
                usort($dataArr['best_rated_courses'], function ($a, $b) {
                    return $b->rating - $a->rating;
                });
                // Sort the 'best_selling_courses' array by 'total_sales' in descending order
                usort($dataArr['best_selling_courses'], function ($a, $b) {
                    return $b->total_sales - $a->total_sales;
                });
            }

            $sortArr = [];
            foreach ($allowArray as $allow) {
                $sortArr[$allow] = $dataArr[$allow] ?? [];
            }

            return $sortArr;
        } catch (\Throwable $th) {
            return false;
        }

    }

    //End Home Api
}
