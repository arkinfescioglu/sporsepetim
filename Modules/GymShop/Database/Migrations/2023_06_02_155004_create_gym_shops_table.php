<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gym_shops', function (Blueprint $table) {
            $table->id();
            $table->string("gym_name");
            $table->string("gym_code")->nullable();
            $table->unsignedBigInteger("company_type_id");
            $table->unsignedBigInteger("city_id")->nullable()->index();
            $table->unsignedBigInteger("district_id")->nullable()->index();
            $table->unsignedBigInteger("street_id")->nullable()->index();
            $table->unsignedBigInteger("neighbourhood_id")->nullable()->index();
            $table->unsignedBigInteger("parent_ie")->nullable()->index();
            $table->string("tax_administration")->nullable();
            $table->string("tax_number")->nullable();
            $table->string("authorized_name", 100)->nullable();
            $table->string("authorized_email", 150)->nullable();
            $table->string("authorized_phone", 50)->nullable();
            $table->string("concat_email", 150)->nullable();
            $table->string("authorized_web", 150)->nullable();
            $table->json("social_media")->nullable();
            $table->integer("number_of_instructors")->default("1");
            $table->integer("square_meters")->default("1");
            $table->integer("maximum_member_capacity")->default("100");
            $table->boolean("is_there_a_branch")->default(false);
            $table->boolean("approved")->default(false);
            $table->longText("gym_properties")->nullable();
            $table->longText("gym_services")->nullable();
            $table->longText("image")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gym_shops');
    }
};
