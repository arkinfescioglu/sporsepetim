<?php

namespace Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GymShop extends Model
{
    use HasFactory;

    protected $fillable = [
        "gym_name",
        "gym_code",
        "company_type_id",
        'city_id',
        'district_id',
        'neighbourhood_id',
        'tax_administration',
        //verg, dairesi,
        'tax_number',
        'authorized_name',
        'authorized_email',
        'authorized_phone',
        'concat_email',
        'authorized_web',
        'social_media',
        'number_of_instructors',
        'square_meters',
        'maximum_member_capacity',
        'is_there_a_branch',
        "parent_ie",
        "approved",
        "gym_properties",
        "gym_services",
        "image",
    ];

    protected $casts = [
        "social_media"      => "array",
        "is_there_a_branch" => "boolean",
        "approved"          => "boolean",
    ];
}
