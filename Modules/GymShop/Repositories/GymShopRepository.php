<?php

namespace Modules\GymShop\Repositories;

use App\Lib\Repository\Abstracts\BaseRepository;
use Modules\GymShop\Interfaces\GymShopInterface;

class GymShopRepository extends BaseRepository implements GymShopInterface
{

    /**
     * Ctor.
     *
     * @param \Entities\GymShop $model
     */
    public function __construct(
        \Entities\GymShop $model
    )
    {
        $this->model = $model;
    }
}
