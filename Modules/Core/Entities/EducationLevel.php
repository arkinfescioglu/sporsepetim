<?php

namespace Modules\GymShop\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\EducationLevel
 *
 * @property int $id
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|EducationLevel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EducationLevel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EducationLevel query()
 * @mixin \Eloquent
 */
class EducationLevel extends Model
{
    use HasFactory;

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        "name"
    ];

    /**
    * The attributes that should be cast to native types.
    *
    * @var array
    */
    protected $casts = [];
}
