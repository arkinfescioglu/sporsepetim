<?php

namespace Modules\GymShop\Database\Seeder;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\GymShop\Entities\CompanyType;

class CompanyTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("company_types")->truncate();
        CompanyType::create([
            "company_type_name" => "Tüzel"
        ]);
        CompanyType::create([
            "company_type_name" => "Şahıs"
        ]);
    }
}
