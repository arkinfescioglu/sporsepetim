<?php

namespace Modules\GymShop\Database\Seeder;

use App\Models\EducationLevel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EducationLevelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("education_levels")->truncate();
        EducationLevel::create([
            "id" => 1,
            "name" => "İlkokul Mezunu"
        ]);
        EducationLevel::create([
            "id" => 2,
            "name" => "Lise Mezunu"
        ]);
        EducationLevel::create([
            "id" => 3,
            "name" => "Üniversite Mezunu"
        ]);
        EducationLevel::create([
            "id" => 4,
            "name" => "Yükse Lisans"
        ]);
        EducationLevel::create([
            "id" => 5,
            "name" => "Doktora"
        ]);
        EducationLevel::create([
            "id" => 6,
            "name" => "Doktora ve üzeri"
        ]);
        EducationLevel::create([
            "id" => 7,
            "name" => "Belirtmek istemiyorum"
        ]);
    }
}
