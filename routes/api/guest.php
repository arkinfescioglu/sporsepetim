<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Api\V1\Guest\HomeController;
use App\Http\Controllers\Api\V1\Guest\CourseCategoryController;



Route::controller(CourseCategoryController::class)->prefix('v1/course')->group(function () {
    Route::get('/categories',                             'categories')->name('student.api.course.categories');
    Route::get('/categories/{id}/details',                'categoryDetails')->name('student.api.course.category_details');
});




